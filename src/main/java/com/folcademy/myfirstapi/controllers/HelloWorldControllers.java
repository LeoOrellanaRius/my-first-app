package com.folcademy.myfirstapi.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController  /* este es el controlador, indica a spring que es un controlador Rest */

public class HelloWorldControllers {
    @RequestMapping("/")
    public String helloWorld(){
        return "Hello World";
    }
}
