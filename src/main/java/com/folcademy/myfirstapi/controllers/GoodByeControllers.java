package com.folcademy.myfirstapi.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodByeControllers {
    @RequestMapping("/goodbye")
    public String GoodByeControllers(){
        return "Chauuu";
    }
}
